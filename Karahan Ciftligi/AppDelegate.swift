//
//  AppDelegate.swift
//  Karahan Ciftligi
//
//  Created by Ucdemir on 19.06.2020.
//  Copyright © 2020 karahanciftligi. All rights reserved.
//

import UIKit
import Firebase
import OneSignal
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {

var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
     
        
        FirebaseApp.configure()
        
        //Remove this method to stop OneSignal Debugging
         OneSignal.setLogLevel(.LL_VERBOSE, visualLevel: .LL_NONE)

         //START OneSignal initialization code
         let onesignalInitSettings = [kOSSettingsKeyAutoPrompt: false, kOSSettingsKeyInAppLaunchURL: false]
         
         // Replace 'YOUR_ONESIGNAL_APP_ID' with your OneSignal App ID.
         OneSignal.initWithLaunchOptions(launchOptions,
           appId: "4783a02d-fa24-4dab-9f73-1db8843e7197",
           handleNotificationAction: nil,
           settings: onesignalInitSettings)

         OneSignal.inFocusDisplayType = OSNotificationDisplayType.notification;

         // The promptForPushNotifications function code will show the iOS push notification prompt. We recommend removing the following code and instead using an In-App Message to prompt for notification permission (See step 6)
         OneSignal.promptForPushNotifications(userResponse: { accepted in
           print("User accepted notifications: \(accepted)")
         })
         //END OneSignal initializataion code

        
        return true
    }




}

